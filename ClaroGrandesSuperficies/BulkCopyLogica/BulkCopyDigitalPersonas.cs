﻿using ClaroGrandesSuperficies.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ClaroGrandesSuperficies.BulkCopyLogica
{
    public class BulkCopyDigitalPersonas
    {

        private static ContextClaroColombia dbClaroColombia = new ContextClaroColombia();

        public void BulkAsignacion(DataTable dtUsuarios)
        {


            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(dbClaroColombia.Database.Connection.ConnectionString))
            {

                bulkcopy.DestinationTableName = "data";

                bulkcopy.ColumnMappings.Add("Year", "Year");
                bulkcopy.ColumnMappings.Add("Month", "Month");
                bulkcopy.ColumnMappings.Add("Segmento", "Segmento");
                bulkcopy.ColumnMappings.Add("TELE_NUMB", "TELE_NUMB");
                bulkcopy.ColumnMappings.Add("CELULAR_1", "CELULAR_1");
                bulkcopy.ColumnMappings.Add("CELULAR_2", "CELULAR_2");
                bulkcopy.ColumnMappings.Add("CELULAR_3", "CELULAR_3");
                bulkcopy.ColumnMappings.Add("TELEFONO_1", "TELEFONO_1");
                bulkcopy.ColumnMappings.Add("TELEFONO_2", "TELEFONO_2");
                bulkcopy.ColumnMappings.Add("TELEFONO_3", "TELEFONO_3");
                bulkcopy.ColumnMappings.Add("CO_ID", "CO_ID");
                bulkcopy.ColumnMappings.Add("NOMBRE_BBDD", "NOMBRE_BBDD");
                bulkcopy.ColumnMappings.Add("REGION", "REGION");
                bulkcopy.ColumnMappings.Add("COD", "COD");
                bulkcopy.ColumnMappings.Add("ID_ASIGNACION", "ID_ASIGNACION");
                bulkcopy.ColumnMappings.Add("ID_DIME", "ID_DIME");



                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);                  


                }
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233079)
                    {
                        throw new Exception(ex.Message);
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }

                }
            }
        }

        public void Insertar()
        {
            try
            {
                SqlConnection connection = new SqlConnection(dbClaroColombia.Database.Connection.ConnectionString);
                dbClaroColombia.Database.CommandTimeout = 32767;

                SqlCommand comandoInsert = new SqlCommand("P_INSERTAR_BASE_DIGITAL_PERSONAS", connection);

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();
                if (reader != null) reader.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error, El archivo no debe superar los 300.000 registros. \n" + ex.Message.ToString());
            }
        }

        public void TruncarTablaTemporal(string Tabla, string opcion)
        {

            try
            {
                SqlConnection connection = new SqlConnection(dbClaroColombia.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("TRUNCATE TABLE " + Tabla, connection);

                comandoInsert.CommandType = CommandType.Text;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

    }
}