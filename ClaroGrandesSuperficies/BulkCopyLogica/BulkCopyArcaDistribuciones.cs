﻿using ClaroGrandesSuperficies.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ClaroGrandesSuperficies.BulkCopyLogica
{
    public class BulkCopyArcaDistribuciones
    {
        private static ContextModel db = new ContextModel();

        /// <summary>
        /// Método que se encarga de hacer la lectura del archivo.
        /// </summary>
        /// <param name="dtUsuarios"></param>

        public void BulkAsignacion(DataTable dtUsuarios)
        {



            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
            {
                bulkcopy.DestinationTableName = "dataCargue";
                bulkcopy.ColumnMappings.Add("AgenteAsignadoId", "AgenteAsignadoId");
                bulkcopy.ColumnMappings.Add("Codigo", "Codigo");
                bulkcopy.ColumnMappings.Add("Identificacion", "Identificacion");
                bulkcopy.ColumnMappings.Add("Nombre_Cliente", "Nombre_Cliente");
                bulkcopy.ColumnMappings.Add("Direccion", "Direccion");
                bulkcopy.ColumnMappings.Add("Telefono", "Telefono");
                bulkcopy.ColumnMappings.Add("Mail", "Mail");
                bulkcopy.ColumnMappings.Add("Nombre_Comun", "Nombre_Comun");
                bulkcopy.ColumnMappings.Add("Sector", "Sector");
                bulkcopy.ColumnMappings.Add("Contacto", "Contacto");
                bulkcopy.ColumnMappings.Add("Activo", "Activo");
                bulkcopy.ColumnMappings.Add("Lista_Precios", "Lista_Precios");
                bulkcopy.ColumnMappings.Add("Creacion", "Creacion");
                bulkcopy.ColumnMappings.Add("Zona", "Zona");
                bulkcopy.ColumnMappings.Add("Canal", "Canal");
                bulkcopy.ColumnMappings.Add("Segmento", "Segmento");
                bulkcopy.ColumnMappings.Add("Cantidad_Dias", "Cantidad_Dias");
                bulkcopy.ColumnMappings.Add("Localidad", "Localidad");
                bulkcopy.ColumnMappings.Add("Barrio", "Barrio");
                bulkcopy.ColumnMappings.Add("Ciudad", "Ciudad");
                bulkcopy.ColumnMappings.Add("Departamento", "Departamento");
                bulkcopy.ColumnMappings.Add("Grupo_Empresa", "Grupo_Empresa");
                bulkcopy.ColumnMappings.Add("Agencia", "Agencia");
                bulkcopy.ColumnMappings.Add("Forma_Pago", "Forma_Pago");
                bulkcopy.ColumnMappings.Add("Gestionado", "Gestionado");
                bulkcopy.ColumnMappings.Add("EstadoBase", "EstadoBase");

                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);

                    // Función que se encarga de insertar en la base de datos.
                    Insertar();


}
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233079)
                    {
                        throw new Exception(ex.Message);
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }

                }
            }
        }

        public void Insertar()
        {
            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);
                db.Database.CommandTimeout = 32767;


                SqlCommand comandoInsert = new SqlCommand("P_CrearClientes", connection);

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error, El archivo no debe superar los 300.000 registros. \n" + ex.Message.ToString());

            }
        }

        public void TruncarTablaTemporal(string Tabla, string opcion)
        {

            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("TRUNCATE TABLE " + Tabla, connection);

                comandoInsert.CommandType = CommandType.Text;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }


    }
}