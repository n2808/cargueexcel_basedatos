﻿using ClaroGrandesSuperficies.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ClaroGrandesSuperficies.BulkCopyLogica
{
    public class BulkCopyTecnologiaHogares
    {
        
        private static ContextHogares dbHogares = new ContextHogares();


        public void BulkAsignacion(DataTable dtUsuarios)
        {


            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(dbHogares.Database.Connection.ConnectionString))
            {
                bulkcopy.BulkCopyTimeout = 0;

                //Base de datos entregada por Mapfre
                bulkcopy.DestinationTableName = "data";
                bulkcopy.ColumnMappings.Add("YEAR", "YEAR");
                bulkcopy.ColumnMappings.Add("MONTH", "MONTH");
                bulkcopy.ColumnMappings.Add("BASE", "BASE");
                bulkcopy.ColumnMappings.Add("SEGMENTO", "SEGMENTO");
                bulkcopy.ColumnMappings.Add("TELE_NUMB", "TELE_NUMB");
                bulkcopy.ColumnMappings.Add("CONTRATO", "CONTRATO");
                bulkcopy.ColumnMappings.Add("CUENTA", "CUENTA");
                bulkcopy.ColumnMappings.Add("ESTADO", "ESTADO");
                bulkcopy.ColumnMappings.Add("NOMBRE_CLIENTE", "NOMBRE_CLIENTE");
                bulkcopy.ColumnMappings.Add("TIPO_IDENT", "TIPO_IDENT");
                bulkcopy.ColumnMappings.Add("NUMERO_IDENTIFICACION", "NUMERO_IDENTIFICACION");
                bulkcopy.ColumnMappings.Add("TIPO_CLIENTE", "TIPO_CLIENTE");
                bulkcopy.ColumnMappings.Add("DIRECCION", "DIRECCION");
                bulkcopy.ColumnMappings.Add("CIUDAD", "CIUDAD");
                bulkcopy.ColumnMappings.Add("ESTRATO", "ESTRATO");
                bulkcopy.ColumnMappings.Add("TELEFONO_1", "TELEFONO_1");
                bulkcopy.ColumnMappings.Add("TELEFONO_2", "TELEFONO_2");
                bulkcopy.ColumnMappings.Add("TELEFONO_3", "TELEFONO_3");
                bulkcopy.ColumnMappings.Add("CELULAR_1", "CELULAR_1");
                bulkcopy.ColumnMappings.Add("CELULAR_2", "CELULAR_2");
                bulkcopy.ColumnMappings.Add("EMAIL", "EMAIL");               
                bulkcopy.ColumnMappings.Add("ID_ASIGNACION", "ID_ASIGNACION");
                bulkcopy.ColumnMappings.Add("ID_DIME", "ID_DIME");

                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);               


}
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233079)
                    {
                        throw new Exception(ex.Message);
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }

                }
            }
        }

        public void Insertar()
        {
            try
            {
                SqlConnection connection = new SqlConnection(dbHogares.Database.Connection.ConnectionString);
                dbHogares.Database.CommandTimeout = 32767;

                SqlCommand comandoInsert = new SqlCommand("P_INSERTAR_BASE_HOGARES_TECNOLOGIA", connection);

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error, El archivo no debe superar los 300.000 registros. \n" + ex.Message.ToString());

            }
        }

        public void TruncarTablaTemporal(string Tabla, string opcion)
        {

            try
            {
                SqlConnection connection = new SqlConnection(dbHogares.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("TRUNCATE TABLE " + Tabla, connection);

                comandoInsert.CommandType = CommandType.Text;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

    }
}