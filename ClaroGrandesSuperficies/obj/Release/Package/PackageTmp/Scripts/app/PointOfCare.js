﻿
class PointOfCare extends General {
    
    constructor() {
        super();
        this.data = {};
    }

    // GET CITIES FROM STATE
    UpdateCities(value , callback) {
        this.data = { State:value };
        var url = this.url;
        $.post(this.url.PointOfCare + 'getCities', this.data)
            .done(Result => {
                $("#CityId").html("");
                this.PopulateDropdown(Result);
                if (typeof callback === "function" ) callback() ;
        })
        .fail( (data, status) => {
            _console("Error interno favor contactar al administrador");
        })

    }

    // GET CUSTOMER DATA
    SearchClient(doc) {
        this.data = { document: doc };
        $.post(this.url.Customer + 'GetCustomer', this.data)
            .done(Result => {
                if (Result != null) {
                    FillClient(Result,1);
                }
            })
            .fail((data, status) => {
                FillClient(null, 0);

            })
    }

    AddProductToSell(Product) {
        this.data = { barCode : Product };
        $.post(this.url.Inventory + 'SearchStock', this.data)
            .done(Result => {
                if (Result != null) {
                    AddProductToTable(Result);
                }
            })
            .fail((data, status) => {
                $(".alert-danger").remove();
                $("#msg_colum").append(string_mensaje("No se encontro el producto buscado", "alert-danger"))

            })
    }

    SaveProduct() {

        var Inventories = [];

        $("#ListProductsToSell tbody tr").each(function (index, elem) {
            Inventories.push($(this).data('id'));
        });
        _console(Inventories);
        if (Inventories.length < 1) {
            $("#box_sales_create").append(string_mensaje("no se ha seleccionado ningun producto", "alert-danger"))
            return;
        }
        this.data = {
            Items: Inventories
            , __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
            , CustomerId: $("#Id").val()
        }
        $(".alert-danger").remove();

        $.post(this.url.Sales + 'Create', this.data)
            .done(Result => {
                if (Result != null) {
                    window.location = this.url.Sales + 'Create';
                    _console(this.url.Sales + 'Details/'+Result);
                }
            })
            .fail((data, status) => {
                _console("error interno ");
                _console(data);
                _console(status);
                if (data.status == 400) {
                    _console("error data");
                    $.each(data.responseJSON, function (val, elm) {
                        _console(elm);
                        _console(elm);
                        _console(Point.JsonDecodeErrors(elm));
                        $("#box_sales_create").append(Point.JsonDecodeErrors(elm));
                    });
                }
            })
    }
};

Point = new PointOfCare()

/*¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬
   EVENTS DEFINITIONS
 ¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬*/

// update the cities by department
$("body").on('input', '#DepartmentId', function(){
    const value = $(this).val();
    if (!value) { return;}
    Point.UpdateCities(value);
})

// search a client for a sell
$("body").on('click', '#SearchClient', function () {
    const value = $("#Input_Document").val();
    if (!value) { return; }
    Point.SearchClient($.trim(value));
})

// Add product to sell
$("body").on('click', '#btn_searchStock', function (event) {
    event.preventDefault();
    const value = $("#Input_Barcode").val();
    Point.AddProductToSell($.trim(value));
})
// End the Sale
$("body").on('click', '#btn_endSale', function (event) {
    event.preventDefault();
    Point.SaveProduct();
})







/*¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬
   GENERIC FUNCTIONS
 ¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬*/

FillClient = (Result, Type) => {

    if (Type == 0) {
        $("#Id, #Document, #Names, #LastNames, #Address, #Phone1, #Phone2, #cityId, #DepartmentId, #CityId").val("");
        $("#Id, #Document, #Names, #LastNames, #Address, #Phone1, #Phone2, #cityId, #DepartmentId, #CityId").attr("readonly", false);
        $("#label_cliente").text("Cliente : " + $("#Input_Document").val() + " No encontrado")
        $("#box_cliente").addClass("box-danger").removeClass(" box-success")
        $("#CreateCustomer").show();
        $("#box_sales_create").hide();
        return;
    }
    $("#label_cliente").text("Cliente : Encontrado")
    $("#box_cliente").removeClass("box-danger").addClass("box-success")
    $('.field-validation-error').text("");
    $("#CreateCustomer").hide();
    $("#Id, #Document, #Names, #LastNames, #Address, #Phone1, #Phone2, #cityId, #DepartmentId, #CityId").attr("readonly", true);
    $("#Id").val(Result.Id);
    $("#Document").val(Result.Document);
    $("#Names").val(Result.Document);
    $("#LastNames").val(Result.LastNames);
    $("#Address").val(Result.Address);
    $("#Phone1").val(Result.Phone1);
    $("#Phone2").val(Result.Phone2);
    $("#cityId").val(Result.cityId);
    $("#DepartmentId").val(Result.DepartmentId);
    Point.UpdateCities(Result.DepartmentId, () => {
        $("#CityId").val(Result.cityId);

    });
    $("#box_sales_create").show();



}


// add products to table.
AddProductToTable = Product => {
    $(".alert-danger").remove();
    if ($("#ListProductsToSell tbody tr[data-id='" + Product.InventoryId + "']").length > 0){
        $("#msg_colum").append(string_mensaje("Este producto ya fue agregado","alert-danger"))
        return;
    }
    var th = `<tr data-id="${Product.InventoryId}"> <td>${Product.type}</td> <td>${Product.ProductName}</td> <td>${Product.Serial}</td> <td>${Product.BarCode}</td> <td>${Product.Price}</td> <td><button class="btn btn-danger btn_removerFila"><i class="glyphicon glyphicon-trash"></i></button></td> </tr>`;
    $("#ListProductsToSell tbody").append(th);
}

