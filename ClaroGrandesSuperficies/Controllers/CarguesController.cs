﻿using ClaroGrandesSuperficies.BulkCopyLogica;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Viewmodel;
using Core.Models.User;
using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class CarguesController : Controller
    {

        private static BulkCopyArcaDistribuciones buldCopyArcaDistribuciones = new BulkCopyArcaDistribuciones();
        private static BulkCopyTecnologiaHogares buldCopyTecnologiaHogares = new BulkCopyTecnologiaHogares();
        private static BulkCopyDigitalPersonas buldCopyDigitalPersonas = new BulkCopyDigitalPersonas();
        private static ContextClaroColombia dbClaroColombia = new ContextClaroColombia();
        private static ContextHogares dbHogaresTecnologia = new ContextHogares();


        // GET: ArcaDistribuciones
        public ActionResult Index()
        {
            return View();
        }

        // Método para insertar los datos.
        public bool InsertarUsuariosAplicativos(string path, string opcion)
        {

            try
            {
                DataTable dtUsuarios = new DataTable();

                // Pasamos el nombre de la tabla que se va a truncar.                
                if (opcion == "1")
                {
                    buldCopyArcaDistribuciones.TruncarTablaTemporal("dataCargue", opcion);
                    dtUsuarios = CargarExcel(path);
                    buldCopyArcaDistribuciones.BulkAsignacion(dtUsuarios);
                    return true;

                }
                else if (opcion == "2")
                {
                    buldCopyTecnologiaHogares.TruncarTablaTemporal("data", opcion);
                    dtUsuarios = CargarExcel(path);
                    buldCopyTecnologiaHogares.BulkAsignacion(dtUsuarios);
                    return true;
                }
                else
                {
                    buldCopyDigitalPersonas.TruncarTablaTemporal("data", opcion);
                    dtUsuarios = CargarExcel(path);
                    buldCopyDigitalPersonas.BulkAsignacion(dtUsuarios);
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable CargarExcel(string path)
        {

            try
            {
                var strconn = ("Provider=Microsoft.ACE.OLEDB.12.0;" +
                ("Data Source=" + (path + ";Extended Properties=\"Excel 12.0;HDR=YES\"")));

                DataTable dataTable = new DataTable();


                OleDbConnection mconn = new OleDbConnection(strconn);
                mconn.Open();
                DataTable dtSchema = mconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if ((null != dtSchema) && (dtSchema.Rows.Count > 0))
                {
                    string firstSheetName1 = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                    string firstSheetName = "Cargue$";
                    new OleDbDataAdapter("SELECT * FROM [" + firstSheetName.Trim() + "]", mconn).Fill(dataTable);
                }
                mconn.Close();

                return dataTable;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        [HttpPost]
        public JsonResult CargarArchivoUsuarios(string opcion)
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    EliminarArchivosResources();

                    string path = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario"),
                                  Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    var dt = InsertarUsuariosAplicativos(path, opcion);

                    return Json(new { status = 201, message = "El archivo se ha cargado correctamente!", opcion = opcion });

                }

                catch (Exception ex)
                {
                    return Json(new { status = 504, message = ex.Message });
                }
            }

            return Json("Por favor seleccione el archivo");
        }


        public void EliminarArchivosResources()
        {
            try
            {
                var pathResources = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario").ToString());

                DirectoryInfo di = new DirectoryInfo(pathResources);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();

                }
            }
            catch (Exception ex)
            {

                throw new Exception();
            }
        }

        [HttpPost]
        public JsonResult CargarDatos(string opcion)
        {
            int dataCount = 0;
            if (opcion.Equals("2"))
            {
                buldCopyTecnologiaHogares.Insertar();
                dataCount = dbHogaresTecnologia.DataHogaresTecnologiaVMs.SqlQuery("SELECT * FROM data").Count();
            }

            if (opcion.Equals("3"))
            {
                buldCopyDigitalPersonas.Insertar();
                dataCount = dbClaroColombia.DataVMs.SqlQuery("SELECT * FROM data").Count();
            }
           

           
            return Json(new { status = 201, message = "El archivo se ha cargado correctamente!", totalRegistros = dataCount });

        }


    }
}
