﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace ClaroGrandesSuperficies.Models.Logs
{
    public class Log: EntityWithIntId
    {
        #region PROPERTIES
        public string Json { get; set; }
        public DateTime? Date { get; set; }
        public Guid? TypeId { get; set; }
        public int? InventoryId { get; set; }
        #endregion

        #region RELATIONS      

        [ForeignKey("TypeId")]
        public Configuration Type { get; set; }
        #endregion
    }
}