﻿using Core.Models.Common;
using Core.Models.configuration;
using Core.Models.location;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Inventory
{
	public class PointOfCare : Entity
	{

        #region PROPERTIES

        [DisplayName("Ciudad")]
        [Required(ErrorMessage ="La ciudad es obligatoria")]
        public Guid CityId { get; set; }

        public Guid? SupervisorId { get; set; }

        [DisplayName("Nombre")]
        public string Name { get; set; }

        [DisplayName("Dirección")]
        public string Address { get; set; }

        [DisplayName("Código")]
        public string Code { get; set; }

        [DisplayName("Estado")]
        public bool Status { get; set; }

        #endregion

        #region RELATIONS
        public List<UserOfPointOfCare> UserOfPointOfCares { get; set; }
        

        [ForeignKey("CityId")]
        [DisplayName("Ciudad")]
        public City City { get; set; }

        [ForeignKey("SupervisorId")]
        [DisplayName("Supervisor")]
        public User.User Supervisor { get; set; }


        #endregion
    }
}