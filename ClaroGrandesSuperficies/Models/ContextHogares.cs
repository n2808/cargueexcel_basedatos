﻿namespace ClaroGrandesSuperficies.Models
{
    using ClaroGrandesSuperficies.Viewmodel;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public class ContextHogares : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'ContextModel' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'ClaroGrandesSuperficies.Models.ContextModel' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'ContextModel'  en el archivo de configuración de la aplicación.
        public ContextHogares()
            : base("name=HogaresConnection")
        {
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }     

        [NotMapped]
        public DbSet<DataHogaresTecnologiaVM> DataHogaresTecnologiaVMs { get; set; }


    }

}