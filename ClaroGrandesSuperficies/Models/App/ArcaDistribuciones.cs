﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClaroGrandesSuperficies.Models.App
{
    public class ArcaDistribuciones
    {
        public int Id { get; set; }
        //public int? CargaId { get; set; }
        public Guid? AgenteAsignadoId { get; set; }
        public string Codigo { get; set; }
        public string Identificacion { get; set; }
        public string Nombre_Cliente { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Mail { get; set; }
        public string Nombre_Comun { get; set; }
        public string Sector { get; set; }
        public string Contacto { get; set; }
        public Guid? Activo { get; set; }
        public string Lista_Precios { get; set; }
        public string Creacion { get; set; }
        public string Zona { get; set; }
        public string Canal { get; set; }
        public string Segmento { get; set; }
        public string Cantidad_Dias { get; set; }
        public string Localidad { get; set; }
        public string Barrio { get; set; }
        public string Ciudad { get; set; }
        public string Departamento { get; set; }
        public string Grupo_Empresa { get; set; }
        public string Agencia { get; set; }
        public Guid? Forma_Pago { get; set; }
        public string Gestionado { get; set; }
        public string FormaPago { get; set; }
        public string FechaAsignacion { get; set; }
        public string EstadoBase { get; set; }
    }
}