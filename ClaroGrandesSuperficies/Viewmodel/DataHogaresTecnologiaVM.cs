﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClaroGrandesSuperficies.Viewmodel
{
    public class DataHogaresTecnologiaVM
    {
        public int ID { get; set; }
        public string YEAR { get; set; }
        public string MONTH { get; set; }
        public string BASE { get; set; }
        public string SEGMENTO { get; set; }
        public string TELE_NUMB { get; set; }
        public string CONTRATO { get; set; }
        public string CUENTA { get; set; }
        public string ESTADO { get; set; }
        public string NOMBRE_CLIENTE { get; set; }
        public string TIPO_IDENT { get; set; }
        public string NUMERO_IDENTIFICACION { get; set; }
        public string TIPO_CLIENTE { get; set; }
        public string DIRECCION { get; set; }
        public string CIUDAD { get; set; }
        public string ESTRATO { get; set; }
        public string TELEFONO_1 { get; set; }
        public string TELEFONO_2 { get; set; }
        public string TELEFONO_3 { get; set; }
        public string CELULAR_1 { get; set; }
        public string CELULAR_2 { get; set; }
        public string EMAIL { get; set; }
        public string ID_ASIGNACION { get; set; }
        public string ID_DIME { get; set; }

    }
}