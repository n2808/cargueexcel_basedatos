﻿namespace ClaroGrandesSuperficies.Viewmodel
{
    public class DataVM
    {
        public int Id { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Segmento { get; set; }
        public string TELE_NUMB { get; set; }
        public string CELULAR_1 { get; set; }
        public string CELULAR_2 { get; set; }
        public string CELULAR_3 { get; set; }
        public string TELEFONO_1 { get; set; }
        public string TELEFONO_2 { get; set; }
        public string TELEFONO_3 { get; set; }
        public string CO_ID { get; set; }
        public string NOMBRE_BBDD { get; set; }
        public string REGION { get; set; }
        public string COD { get; set; }
        public string ID_ASIGNACION { get; set; }
        public string ID_DIME { get; set; }
    }
}