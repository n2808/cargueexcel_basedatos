﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClaroGrandesSuperficies.Viewmodel
{
    public class GeneralVM
    {
    }

    public class CustoErrors
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public CustomErrorType Type { get; set; }
    }

    public enum CustomErrorType
    {
       Nofound,
       NoAvailable
    }

    // STATUS OF PRODUCTS
    public static class ProductStatus
    {
        public static Guid Available { get; set; } = new Guid("e20f962f-e7e7-469a-aecb-774daef26fa6");
        public static Guid Sold { get; set; } = new Guid("4e021586-95d8-4252-923f-b01d1eaa80b8");
        public static Guid InSale { get; set; } = new Guid("639a8a6e-b3b2-4a39-9b09-33ee45558052");
        public static Guid InTransfer { get; set; } = new Guid("f8f1c9d3-c01b-4472-acb5-2e7d5593b149");
    }

    //STATUS OF MOVEMENTS
    public class MovementsStatus
    {
        public Guid RquesSent { get; set; } = new Guid("2bc4992c-dd50-4823-9f2a-cf4da78264de");
        public Guid InDeal { get; set; } = new Guid("f3cab593-64af-467d-b1cc-1f8e3e17205f");
        public Guid InRepaid { get; set; } = new Guid("677448f4-33a1-45c9-a257-2c4f67ab29fb");
        public Guid Delivered { get; set; } = new Guid("507779aa-4dac-4799-9188-f78b8aab668c");
        public Guid Canceled { get; set; } = new Guid("FAEDA070-760A-4635-B380-F231E548D42A");
        public Guid Approved { get; set; } = new Guid("ACD7D1A6-DF95-4EFF-B677-E772B19F076F");
        public Guid returnedAndReceived { get; set; } = new Guid("D7381F11-470E-4982-9940-B464FEF8D941");

    }

    // ROLES AND AUTORIZATIONS
    public class RolsStrings
    {
        public string Supervisor { get; set; } = "7E6B69E8-87D0-419F-9054-02FCA94D5A73";
        public string Vendedor { get; set; } = "27436A5D-B3C1-4ED0-A14A-0FAFEA5AB2A5";
        public string CoordinadorZona { get; set; } = "2B88F09C-FE58-4831-9B02-38AFC24F83E3";
        public string Admin { get; set; } = "AF0A50DB-A361-4CA6-9A61-800BFAE10476";
        public string Seguimiento { get; set; } = "90DC8662-076A-451F-9665-8AB3C0687DD5";
    }

    	
}