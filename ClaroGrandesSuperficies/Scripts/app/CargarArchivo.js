﻿
// javascript para la carga del archivo.
$('body').on('click', '#cargarUsuarios', function () {

    if (window.FormData == undefined)
        alert("Error: FormData is undefined");

    else {
        var fileUpload = $("#fileUsuario").get(0);
        var files = fileUpload.files;

        if (files.length === 0) {          

            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'por favor seleccionar el archivo.',
                showConfirmButton: false,
                timer: 1800
            })

        } else {

            let opcion = $('#seleccion').val();

            if (opcion === "" || opcion == undefined) {

                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Seleccione la opción a cargar.',
                    showConfirmButton: false,
                    timer: 1800
                });

                return false;

            }
            
            $('#loading').css('display', 'block');
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);
            fileData.append('opcion', opcion);

        

            $.ajax({
                url: '/Cargues/CargarArchivoUsuarios',
                type: 'post',              
                datatype: 'json',
                contentType: false,
                processData: false,
                async: true,
                data: fileData,
                success: function (response) {
                    if (response.status === 201) {

                        var dataOpcion = new FormData();                        
                        dataOpcion.append('opcion', response.opcion);

                           
                        Swal.fire({
                            title: 'Desea cargar los datos a la tabla original?',
                            text: response.message,
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Insertar Datos!'
                           
                        }).then((result) => {
                            
                            /* Read more about isConfirmed, isDenied below */
                            if (result.value) {

                                $.ajax({
                                    url: '/Cargues/CargarDatos',
                                    type: 'post',                                 
                                    datatype: 'json',
                                    contentType: false,
                                    processData: false,
                                    async: true,
                                    data: dataOpcion,
                                    success: function (response) {
                                        if (response.status === 201) {

                                            Swal.fire('Se han insertado ' + response.totalRegistros + ' registros', '', 'success')


                                        } else {

                                            Swal.fire('Los registros no fueron insertados', '', 'Error')


                                        }
                                    }
                                });

                            } else {
                                Swal.fire('Los registros fueron insertados en la tabla temporal', '', 'success')
                            }
                        })



                        $("#mostrarErrores").css("display", "none");
                        $('#loading').css('display', 'none');
                        limpiarFileInput("fileUsuario");

                        $('#formatoPersonasTecnologia').css('display', 'none');
                        $('#formatoPersonas').css('display', 'none');
                        $('#formatoArca').css('display', 'none');

                    } else {
                      
                        Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 3000
                        })

                        if (response.message.includes('Error Formato')) {
                            $("#mostrarErrores").css("display", "block");
                        }
                        limpiarFileInput("fileUsuario");
                        $('#loading').css('display', 'none');

                    }
                }
            });
        }
    }

});


function limpiarFileInput(idFileInput) {
    document.getElementById(idFileInput).value = "";
}

$('body').on('change', '#seleccion', function () {

    let valor = $(this).val();
    

    if (valor == "1") {
        $('#formatoPersonasTecnologia').css('display', 'none');
        $('#formatoPersonas').css('display', 'none');
        $('#formatoArca').css('display', 'block');
    }

    if (valor == "2") {
        $('#formatoPersonas').css('display', 'none');
        $('#formatoArca').css('display', 'none');
        $('#formatoPersonasTecnologia').css('display', 'block');
    }

    if (valor == "3") {
        $('#formatoArca').css('display', 'none');
        $('#formatoPersonasTecnologia').css('display', 'none');
        $('#formatoPersonas').css('display', 'block');
    }
      
});








