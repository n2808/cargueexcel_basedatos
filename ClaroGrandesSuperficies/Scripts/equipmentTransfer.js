﻿var pointOfCare = null;
// Ocultar campos.
$(document).ready(function () {
    $('.contentInputsTransfer').css('display', 'none');

    pointOfCare = $('#pointOfCareOrigin').val();
    $('#pointOfCareOrigin').change();
    
});

$('body').on('change', '#pointOfCareOrigin', function () {

    pointOfCare = $(this).val();

    $.post('/OrderIncomes/' + 'GetProducts', { PointOfCareId: pointOfCare })
        .done(function (Result) {

            if (Result.status == 200) {

                $('.contentInputsTransfer').css('display', 'block');
                var tablaCuerpo = $("#equipmentList tbody");
                tablaCuerpo.html("<tr style='background:#ccc;'>\
                                <th><div class='form-group form-check'><input type='checkbox' id='checkAll'>&nbsp;&nbsp;Acción</th>\
                                <th>Serial</th>\
                                <th>Nombre</th>\
                                <th>Sede</th>\
                                </tr>");

                for (var i = 0; i < Result.data.length; i++) {

                    var fila = "<tr class='checkFila'>\
                                <td><div class='form-group form-check'><input type='checkbox'  value='" + Result.data[i].Id + "' id='checkTransfer" + i + "' class='form-check-input'></td>\
                                <td>"+ Result.data[i].Serial + "</td>\
                                <td>"+ Result.data[i].Product.Name + "</td>\
                                <td>"+ Result.data[i].PointsOfCare.Name + "</td>\
                                </tr>";

                    tablaCuerpo.append(fila);

                }


            } else {

                Swal.fire({
                    icon: 'info',
                    title: 'La sede seleccionada no posee equipos',
                    showConfirmButton: false,
                    timer: 1500
                })
                    .then(() => {
                        window.location.href = "/Sales/Home";

                    });
            }

        });
});

// Función para hacer check en todos los input.
$('body').on('change', '#checkAll', function () {

    if ($(this).is(':checked')) {

        $(".form-check-input").prop("checked", true);

    } else {

        $(".form-check-input").prop("checked", false);

    }

});

// Proceso para el traslado de los equipos
$('body').on('click', '#btn_transferEquipment', function () {


    var userAuthorizesTransferId = $('#UserAuthorizesTransferId').val();
    var pointOfCareOrigin = $('#pointOfCareOrigin').val();
    var pointOfCareDestination = $('#pointOfCareDestination').val();
    var date = $('#Date').val();
    var userWhoTransferId = $('#UserWhoTransferId').val();


    // #region Validación de Campos.
    if (userAuthorizesTransferId === "" || userAuthorizesTransferId === undefined) {
        $('#errorUserAuthorizesTransferId').text('El campo Usuario que autoriza el traslado es requerido').css('color', 'red');
        return;
    }
    if (pointOfCareOrigin === "" || pointOfCareOrigin === undefined) {
        $('#errorPointOfCareOrigin').text('El campo sede de origen es requerido').css('color', 'red');
        return;
    }
    if (pointOfCareDestination === "" || pointOfCareDestination === undefined) {
        $('#errorPointOfCareDestination').text('El campo sede de destino es requerido').css('color', 'red');
        return;
    }
    if (date === "" || date === undefined) {
        $('#errorDate').text('El campo Fecha de Traslado es requerido').css('color', 'red');
        return;
    }
    if (userWhoTransferId === "" || userWhoTransferId === undefined) {
        $('#errorUserWhoTransferId').text('El campo Usuario que traslada es requerido').css('color', 'red');
        return;
    }

    //#endregion
    var data = {
        InventoryId: [],
        UserAuthorizesTransferId: userAuthorizesTransferId,
        PointOfCareOrigin: pointOfCareOrigin,
        PointOfCareDestination: pointOfCareDestination,
        Date: date,
        UserWhoTransferId: userWhoTransferId
    }

    $(".form-check-input").each(function (index) {
        if ($(this).is(':checked')) {

            data.InventoryId.push($(this).val());

        }
    });

    if (data.InventoryId.length <= 0) {
        Swal.fire({
            icon: 'info',
            title: 'Debe seleccionar el dispositivo a trasladar',
            showConfirmButton: false,
            timer: 1500
        });
        return false;
    }



    $.post('/OrderIncomes/' + 'InventoryTransfer', data)
        .done(function (Result) {

            console.log(Result);

            if (Result.status == 200) {

                Swal.fire({
                    icon: 'success',
                    title: 'Traslado  Exitoso',
                    showConfirmButton: false,
                    timer: 1500
                })
                    .then(() => {
                        location.reload(true);
                    });

            } else {

                Swal.fire({
                    icon: 'info',
                    title: 'Debe seleccionar el dispositivo a trasladar',
                    showConfirmButton: false,
                    timer: 1500
                });

            }


        });
});


// #region  Procedimiento para quitar las alertas.
$('body').on('change', '#UserAuthorizesTransferId', function () {
    $('#errorUserAuthorizesTransferId').text('');
});
$('body').on('change', '#pointOfCareOrigin', function () {
    $('#errorPointOfCareOrigin').text('');
});
$('body').on('change', '#pointOfCareDestination', function () {
    $('#errorPointOfCareDestination').text('');
});
$('body').on('change', '#Date', function () {
    $('#errorDate').text('');
});
$('body').on('blur', '#UserWhoTransferId', function () {
    $('#errorUserWhoTransferId').text('');
});
//#endregion
